package datastructure;

import cellular.CellState;

public class CellGrid implements IGrid {
    private int rows;
    private int columns;
    private CellState[][]cellGrid;

    public CellGrid(int rows, int columns, CellState initialState) {
		// TODO Auto-generated constructor stub
        this.rows = rows;
        this.columns = columns;

        //private Array<CellState> initialState;
        cellGrid = new CellState[rows][columns];
        
        for(int i = 0; i < rows; i++){
            for(int j = 0; j < columns; j++){
                cellGrid[i][j] = initialState;
    
            }
        }
       
	}

    @Override
    public int numRows() {
        // TODO Auto-generated method stub
        return rows;
    }

    @Override
    public int numColumns() {
        // TODO Auto-generated method stub
        return columns;
    }

    @Override
    public void set(int row, int column, CellState element) {
        // TODO Auto-generated method stub
        if(row < 0 || row > rows){
            throw new IndexOutOfBoundsException("Row can't have a negative value or bigger then given row");
        }
        if(column < 0 || column > columns){
            throw new IndexOutOfBoundsException("Column can't have a negative value or bigger then given column");
        }

        cellGrid[row][column] = element;
    }

    @Override
    public CellState get(int row, int column) {
        // TODO Auto-generated method stub
        if(row < 0 || row > rows){
            throw new IndexOutOfBoundsException("Row can't have a negative value or bigger then given row");
        }
        if(column < 0 || column > columns){
            throw new IndexOutOfBoundsException("Column can't have a negative value or bigger then given column");
        }

        return cellGrid[row][column];
    }

    @Override
    public IGrid copy() {
        // TODO Auto-generated method stub
       CellGrid CellGridCopy = new CellGrid(rows, columns, CellState.DEAD);

        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                CellGridCopy.set(i,j,get(i,j));
            }
        }
        return CellGridCopy;
    }
    
}
